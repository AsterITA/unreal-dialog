// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Unreal_DialogGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UNREAL_DIALOG_API AUnreal_DialogGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
